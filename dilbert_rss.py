#!/usr/bin/env python

"""
Dilbert RSS Feed Generator
Requirements: BeautifulSoup, PyRSS2Gen
http://github.com/fredley/dilbert-rss

"""

import urllib2
import datetime
import sys
from bs4 import BeautifulSoup
import PyRSS2Gen


class getDilbert():
    def __init__(self, output_file):
        self.output = output_file
        self.url = 'http://dilbert.com'
        self.num_items = 10
        self.page = urllib2.urlopen(self.url).read()
        self.soup = BeautifulSoup(self.page)
        self.nextUrl = self.url + self.soup.findAll('div', {'class': 'STR_Image'})[0].find('a')['href']
        self.strips = []
        self.get_strips()
        self.rss = ''
        self.construct_rss()

    def get_strips(self):
        for i in range(0, self.num_items):
            details = self.get_details(self.nextUrl, self.url)
            self.strips.append(details['item'])
            self.nextUrl = self.url + details['prev_href']

    def get_details(self, url, base_url):
        page = urllib2.urlopen(url).read()
        soup = BeautifulSoup(page)
        date = soup.findAll('div', {'class': 'STR_DateStrip'})[0].text
        img = soup.findAll('div', {'class': 'STR_Image'})[0].find('img')['src'].replace('sunday.', '') \
            .replace('strip.gif', 'strip.zoom.gif')
        results = {}
        results['item'] = PyRSS2Gen.RSSItem(
            title='Comic for ' + date,
            description="<a href='" + url + "'><img src='" + base_url + str(img) + "' /></a>",
            pubDate=datetime.datetime.strptime(date, '%B %d, %Y'),
            link=url,
            guid=PyRSS2Gen.Guid(url)
        )
        print soup.findAll('span', text='Previous')[0].parent['href']
        results['prev_href'] = soup.findAll('span', text='Previous')[0].parent['href']
        return results

    def construct_rss(self):
        self.rss = PyRSS2Gen.RSS2(
            title="Dilbert Daily Strip",
            link="http://dilbert.com",
            description="An unofficial RSS feed for dilbert.com.",
            lastBuildDate=datetime.datetime.now(),
            items=self.strips)
        self.rss.write_xml(open(self.output, "w"))
        return

if __name__ == '__main__':
    if len(sys.argv) > 1:
        outfile = sys.argv[1]
    else:
        outfile = 'dilbert.xml'
    x = getDilbert(outfile)
    x.construct_rss()

