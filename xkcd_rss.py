#!/usr/bin/env python

"""
xkcd.com RSS Feed Generator
Requirements: BeautifulSoup, PyRSS2Gen

"""

import urllib2
import datetime
import sys
import json
import PyRSS2Gen
import urlparse


class getXKCD():
    def __init__(self, output_file):
        self.output = output_file
        #'http://xkcd.com/{0}/info.0.json'
        self.url = 'http://xkcd.com'
        self.url_part = 'info.0.json'
        self.url_new = urlparse.urljoin(self.url, self.url_part)
        self.num_items = 10
        self.strips = []
        self.rss = None
        self.get_strips()
        self.construct_rss()

    def get_strips(self):
        nextUrl = self.url_new
        for i in range(0, self.num_items):
            details = self.get_details(nextUrl)
            self.strips.append(details['item'])
            nextUrl = details['prev_href']

    def get_details(self, url):
        page = urllib2.urlopen(url).read()
        json_dict = json.loads(page)
        date = '/'.join([json_dict['month'], json_dict['day'], json_dict['year']])
        date = datetime.datetime.strptime(date, '%m/%d/%Y')
        date_title = date.strftime('%B %d, %Y')
        comic_num = json_dict['num']
        title = json_dict['title']
        alt_text = json_dict['alt']
        url = urlparse.urljoin(self.url, str(comic_num), self.url_part)
        print date_title
        img_url = json_dict['img']
        results = {}
        results['item'] = PyRSS2Gen.RSSItem(
            title='{} ({})'.format(title, date_title),
            description="<a href='" + url + "'><img src='" + img_url + "' /></a><p>" + alt_text + "</p>",
            pubDate=date,
            link=url,
            guid=PyRSS2Gen.Guid(url)
        )
        results['prev_href'] = "/".join([self.url, str(int(comic_num) - 1), self.url_part])
        return results

    def construct_rss(self):
        self.rss = PyRSS2Gen.RSS2(
            title="xkcd.com",
            link=self.url,
            description="An unofficial RSS feed for xkcd",
            lastBuildDate=datetime.datetime.now(),
            items=self.strips)
        self.rss.write_xml(open(self.output, "w"))
        return

if __name__ == "__main__":
    if len(sys.argv) > 1:
        outfile = sys.argv[1]
    else:
        outfile = "xkcd.xml"
    x = getXKCD(outfile)