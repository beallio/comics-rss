#!/usr/bin/env python

"""
Cyanide & Happiness RSS Feed Generator
Requirements: BeautifulSoup, PyRSS2Gen

"""

import urllib2
import datetime
import sys
from bs4 import BeautifulSoup
import re
import PyRSS2Gen
import urlparse


class getCNH():
    def __init__(self, output_file):
        self.output = output_file
        self.url = 'http://explosm.net/'
        self.url_new = urlparse.urljoin(self.url, '/comics/new/')
        self.num_items = 10
        self.strips = []
        self.rss = None
        self.skip = False
        self.get_strips()
        self.construct_rss()

    def get_image_url(self, s):
        parsed = s.findAll('div', {'style': 'overflow: auto;'})[1].next_element.contents[2]
        image_url = re.search('(?P<url>h([\w:.\/-])+)', parsed).group('url')
        print image_url
        return image_url

    def get_strips(self):
        nextUrl = self.url_new
        for i in range(0, self.num_items):
            details = self.get_details(nextUrl)
            if not self.skip:
                self.strips.append(details['item'])
            nextUrl = urlparse.urljoin(self.url, details['prev_href'])

    def get_details(self, url):
        page = urllib2.urlopen(url).read()
        soup = BeautifulSoup(page)
        date = soup.findAll('nobr')[0].find('br').next_element
        results = {}
        self.skip = len(date) == 1
        if not self.skip:
            date = datetime.datetime.strptime(date, '%m.%d.%Y')
            date_title = date.strftime('%B %d, %Y')
            print date_title
            img_url = self.get_image_url(soup)
            results['item'] = PyRSS2Gen.RSSItem(
            title='Comic for ' + date_title,
            description="<a href='" + url + "'><img src='" + img_url + "' /></a>",
            pubDate=date,
            link=url,
            guid=PyRSS2Gen.Guid(url)
            )
        results['prev_href'] = soup.findAll('a', {'rel': 'prev'})[0].attrs['href']
        return results

    def construct_rss(self):
        self.rss = PyRSS2Gen.RSS2(
            title="Cyanide & Happiness Comic",
            link=self.url,
            description="An unofficial RSS feed for Cyanide & Happiness.",
            lastBuildDate=datetime.datetime.now(),
            items=self.strips)
        self.rss.write_xml(open(self.output, "w"))
        return

if __name__ == "__main__":
    if len(sys.argv) > 1:
        outfile = sys.argv[1]
    else:
        outfile = "cyanidenhappiness.xml"
    x = getCNH(outfile)