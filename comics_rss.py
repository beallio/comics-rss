#!/usr/bin/env python

import cnh_rss
import dilbert_rss
import xkcd_rss
import os

apache_base_dir = os.path.expanduser('/var/www/rss')
cnh_output = os.path.join(apache_base_dir, 'cyanidenhappiness.xml')
dilbert_ouput = os.path.join(apache_base_dir, 'dilbert.xml')
xkcd_ouput = os.path.join(apache_base_dir, 'xkcd.xml')

cnh_rss.getCNH(cnh_output)
dilbert_rss.getDilbert(dilbert_ouput)
xkcd_rss.getXKCD(xkcd_ouput)